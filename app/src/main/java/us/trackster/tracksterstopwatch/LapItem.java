package us.trackster.tracksterstopwatch;

import java.util.Locale;

import static java.lang.Math.abs;

public class LapItem{
    private long startTime, currentTime, lastLap;
    private String lapTime, diff;
    private boolean modeMilliseconds;
    public LapItem(long startTime, long currentTime, long lastLap, boolean modeMilliseconds){
        this.startTime = startTime;
        this.currentTime = currentTime;
        this.lastLap = lastLap;

        this.lapTime = getTimeString(currentTime-startTime);
        this.diff = getDiffString(lastLap, currentTime);
        this.modeMilliseconds = modeMilliseconds;
    }
    public String getLapTime(){
        return lapTime;
    }
    public String getDiff(){
        return diff;
    }
    public void setStartTime(long startTime){
        this.startTime = startTime;
        lapTime = getTimeString(currentTime-startTime);
    }
    public void setModeMilliseconds(boolean modeMilliseconds){
        this.modeMilliseconds = modeMilliseconds;
        lapTime = getTimeString(currentTime-startTime);
        diff = getDiffString(lastLap, currentTime);
    }

    public String getTimeString(long time){
        int seconds = (int) (time / 1000);
        int minutes = seconds / 60;
        seconds = seconds % 60;
        int milliseconds = (int) (time % 1000);

        String string = String.format(Locale.US,"%02d", minutes) + ":" + String.format(Locale.US,"%02d", seconds) + ".";
        if(modeMilliseconds)
            string += String.format(Locale.US,"%03d", milliseconds);
        else
            string += String.format(Locale.US,"%02d", milliseconds /10);

        return string;

    }

    public String getDiffString(long time1, long time2){
        long diff = abs(time2-time1);
        return "+ " + getTimeString(diff);
    }
}
