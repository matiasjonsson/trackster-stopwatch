package us.trackster.tracksterstopwatch;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import java.util.*;

import static java.lang.Math.*;

public class StopwatchActivity extends AppCompatActivity {

    protected boolean isRunning=false, isRestRunning=false;
    protected boolean buttonDownHeldDown=false, buttonUpHeldDown=false;
    protected TextView timer, rest, lap;
    protected LinearLayout repButtonsList;
    protected HorizontalScrollView scrollViewRepList;
    protected ConstraintLayout stopwatchActivity, resetAndEditTimer;
    protected ArrayList<ArrayList<Long>> laps;
    protected ArrayList<ArrayList<Long>> times;
    protected final int BUTTON_ID_BASE = 3141592, LISTVIEW_ID_BASE=2717182;
    //times format: start, stop, end of rest

    protected final int START_TIME = 0, STOP_TIME = 1, END_OF_REST=2;
    protected final int MESSAGE_DISPLAY_LENGTH=4000;
    protected long currentTime;
    protected Handler handler;
    protected int RepNumber=-1, LapNumber=-1, StopWatchNumber=-1, RepSelected=RepNumber;
    protected ArrayList<ListView> lapLists = new ArrayList<>();
    protected ArrayList<LapItemAdapter> lapItemAdapters = new ArrayList<>();
    protected ArrayList<ArrayList<LapItem>> lapItemArrayLists = new ArrayList<>();
    protected boolean modeMilliseconds = false;
    protected int eightDP;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);

        laps = new ArrayList<>();
        times = new ArrayList<>();

        timer = (TextView)findViewById(R.id.timer);
        rest = (TextView)findViewById(R.id.rest);
        lap = (TextView)findViewById(R.id.lap);
        repButtonsList = (LinearLayout)findViewById(R.id.replist);
        scrollViewRepList = (HorizontalScrollView)findViewById(R.id.scrollviewreplist);
        stopwatchActivity = (ConstraintLayout)findViewById(R.id.activity_stopwatch);
        eightDP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        resetAndEditTimer = (ConstraintLayout)findViewById(R.id.resetandedittimer);

        addRep();
        clockReset(0);

        handler = new Handler();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake(int count) {
                if(isRunning||isRestRunning||StopWatchNumber>0){
                    vibrate(200);
                    shake(count);
                }
            }
        });

        //clockTask = new ClockTask();
        //clockTask.execute();

    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        switch(event.getAction()){
            case KeyEvent.ACTION_DOWN: {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_VOLUME_UP:
                        if (!buttonUpHeldDown) {
                            buttonUpHeldDown = true;
                            clockLap();
                            //new ClockLapTask().execute();
                        }
                        return true;
                    case KeyEvent.KEYCODE_VOLUME_DOWN:
                        if (!buttonDownHeldDown) {
                            buttonDownHeldDown = true;
                            clockStartStop();
                        }
                        return true;
                    default:
                        return super.dispatchKeyEvent(event);
                }
            }
            case KeyEvent.ACTION_UP: {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_VOLUME_UP:
                        if (buttonUpHeldDown) {
                            buttonUpHeldDown = false;
                        }
                        return true;
                    case KeyEvent.KEYCODE_VOLUME_DOWN:
                        if (buttonDownHeldDown) {
                            buttonDownHeldDown = false;
                        }
                        return true;
                    default:
                        return super.dispatchKeyEvent(event);
                }
            }
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public void onDestroy() {
        //clockTask.cancel(true);
        super.onDestroy();
    }

    public void clockStartStop(){
        if(!isRunning){
            clockStart();
        }
        else{
            clockStop();
            //new ClockStopTask().execute();
        }
    }
    public void clockStart(){
        if(RepSelected != RepNumber)
            selectRep(RepNumber);
        currentTime = SystemClock.uptimeMillis();
        if(isRestRunning){
            times.get(RepNumber).set(END_OF_REST, currentTime);
            rest.setText(getTimeString(0L));
            addRep();
        }
        times.get(RepNumber).set(START_TIME, currentTime);
        handler.postDelayed(runnable, 0);
        isRunning = true;
        isRestRunning = false;

    }

    public void clockStop(){
        clockLap();
        if(RepSelected!=RepNumber)
            selectRep(RepNumber);
        //TimeBuff += MillisecondTime;
        currentTime = SystemClock.uptimeMillis();
        times.get(RepNumber).set(STOP_TIME, currentTime);
        //handler.removeCallbacks(runnable);
        isRunning = false;
        isRestRunning = true;
    }

    public void clockLap(){
        if(isRunning){
            if(RepSelected!=RepNumber)
                selectRep(RepNumber);

            currentTime = SystemClock.uptimeMillis();

            if(LapNumber!=-1)
                lapItemArrayLists.get(RepSelected).add(new LapItem(timer.getText().toString(), getDiffString(laps.get(RepNumber).get(LapNumber), currentTime)));
            else
                lapItemArrayLists.get(RepSelected).add(new LapItem(timer.getText().toString(),getString(R.string.nulldiff)));

            laps.get(RepNumber).add(currentTime);
            LapNumber++;
            lapItemAdapters.get(RepSelected).notifyDataSetChanged();
            lapLists.get(RepSelected).smoothScrollToPosition(lapLists.get(RepSelected).getBottom());
        }
    }

    public void deleteLap(int repFromWhichToDelete, int lapToDelete){
        selectRep(repFromWhichToDelete);
        //int numOfLapsInRep = lapItemArrayLists.get(repFromWhichToDelete).size()-1;
        lapItemArrayLists.get(repFromWhichToDelete).remove(lapToDelete);
        lapItemAdapters.get(repFromWhichToDelete).notifyDataSetChanged();
        laps.get(repFromWhichToDelete).remove(lapToDelete);
        LapNumber--;
    }

    public void addRep(){
        RepNumber++;
        LapNumber = -1;
        laps.add(new ArrayList<Long>());
        times.add(new ArrayList<Long>());
        for(int i = 0; i<3; i++)
            times.get(RepNumber).add(0L);

        Button b = new Button(this);
        String repText = "Rep "+String.valueOf(RepNumber+1);
        b.setText(repText);
        b.setAllCaps(false);
        b.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        b.setTextSize(TypedValue.COMPLEX_UNIT_DIP,25);
        b.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
        b.setTextColor(getResources().getColor(R.color.white));
        b.setBackground(getDrawable(R.color.colorPrimary));
        b.setId(BUTTON_ID_BASE+RepNumber);
        b.setPadding(2*eightDP,eightDP,2*eightDP,eightDP);
        b.setElevation(0);
        b.setOnClickListener(new Button.OnClickListener(){
            int id = RepNumber;
            @Override
            public void onClick(View view){
                selectRep(id);
            }
        });
        repButtonsList.addView(b);

        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,ConstraintLayout.LayoutParams.MATCH_CONSTRAINT);
        layoutParams.setMargins(eightDP,0, eightDP,0);
        ListView l = new ListView(this);
        l.setDividerHeight(0);
        l.setDivider(null);
        l.setLayoutParams(layoutParams);
        l.setId(LISTVIEW_ID_BASE+RepNumber);
        LayoutTransition transition = new LayoutTransition();
        l.setLayoutTransition(transition);
        lapLists.add(l);

        lapItemArrayLists.add(new ArrayList<LapItem>());
        lapItemAdapters.add(new LapItemAdapter(StopwatchActivity.this, lapItemArrayLists.get(RepNumber)));

        selectRep(RepNumber);
    }

    public void deleteRep(int repToDelete){
        repButtonsList.removeView((Button)findViewById(BUTTON_ID_BASE+repToDelete));
        stopwatchActivity.removeView((ListView)findViewById(LISTVIEW_ID_BASE+repToDelete));
        Button b;
        for(int i = repToDelete+1; i<=RepNumber; i++){
            b=((Button)findViewById(BUTTON_ID_BASE+i));
            final int id = i;
            b.setId(BUTTON_ID_BASE+i-1);
            b.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View view){
                    selectRep(id-1);
                }
            });
            b.setText("Rep ".concat(String.valueOf(id)));
            lapLists.get(i).setId(LISTVIEW_ID_BASE+i-1);
        }
        times.remove(repToDelete);
        laps.remove(repToDelete);
        lapLists.remove(repToDelete);
        lapItemAdapters.remove(repToDelete);
        lapItemArrayLists.remove(repToDelete);

        RepNumber--;
        if(repToDelete<=RepSelected){
            RepSelected--;
            selectRep(RepSelected);
        }
        LapNumber=laps.get(RepSelected).size()-1;
        isRestRunning=true;
        isRunning=false;
        timer.setText(getTimeString(times.get(RepNumber).get(STOP_TIME)-times.get(RepNumber).get(START_TIME)));
    }

    public void selectRep(int selectedRep){
        if(RepSelected!=-1){
            ListView listToHide = (ListView)findViewById(LISTVIEW_ID_BASE+RepSelected);
            listToHide.setVisibility(View.GONE); //hide old listview

            Button old = (Button)findViewById(BUTTON_ID_BASE+RepSelected);
            old.setBackground(getDrawable(R.color.colorPrimaryDark)); //decolor old rep

            Button current = (Button)findViewById(BUTTON_ID_BASE+RepNumber);
            //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(current.getLayoutParams());
            //layoutParams.setMargins(eightDP,eightDP,eightDP,eightDP);
            //current.setLayoutParams(layoutParams);
            //current.setBackgroundColor(getResources().getColor(R.color.colorPrimaryMediumDark));
            ShapeDrawable shapedrawable = new ShapeDrawable();
            shapedrawable.setShape(new RectShape());
            shapedrawable.getPaint().setColor(getResources().getColor(R.color.colorPrimaryDark));
            shapedrawable.getPaint().setStrokeWidth(45f);
            shapedrawable.getPaint().setStyle(Paint.Style.STROKE);
            current.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            current.setBackground(shapedrawable);  //light tints current rep by adding dark border on light listview background
        }
        RepSelected = selectedRep;
        ListView listToShow;
        if(findViewById(LISTVIEW_ID_BASE+RepSelected)==null){
            listToShow = lapLists.get(RepSelected);
            stopwatchActivity.addView(listToShow);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(stopwatchActivity);
            constraintSet.connect(LISTVIEW_ID_BASE+RepNumber,ConstraintSet.TOP,R.id.cardView,ConstraintSet.BOTTOM);
            constraintSet.connect(LISTVIEW_ID_BASE+RepNumber,ConstraintSet.BOTTOM,R.id.activity_stopwatch,ConstraintSet.BOTTOM);
            constraintSet.applyTo(stopwatchActivity);
        }
        else{
            listToShow = (ListView)findViewById(LISTVIEW_ID_BASE+RepSelected);
            listToShow.setVisibility(View.VISIBLE);
        }
        listToShow.setAdapter(lapItemAdapters.get(RepSelected));

        final Button selected = (Button)findViewById(BUTTON_ID_BASE+RepSelected);

        selected.setTextColor(getResources().getColor(R.color.white));
        selected.setBackground(getDrawable(R.color.colorPrimary)); //color new rep
        scrollViewRepList.post(new Runnable() {
            @Override
            public void run() {int vLeft = selected.getLeft();
                int vRight = selected.getRight();
                int sWidth = scrollViewRepList.getWidth();
                scrollViewRepList.smoothScrollTo(((vLeft + vRight - sWidth) / 2), 0);
            }
        });

    }

    public void shake(int count){
        if(resetAndEditTimer.getVisibility()==View.GONE)
            resetAndEditTimer.setVisibility(View.VISIBLE);
        else
            resetAndEditTimer.setVisibility(View.GONE);
    }

    public void undo(View view){
        Snackbar.make(stopwatchActivity,"Undoing most recent action.",MESSAGE_DISPLAY_LENGTH).show();
        mShakeDetector.resetCount();
        vibrate(500);
        if(isRestRunning){
            times.get(RepNumber).set(STOP_TIME,0L);
            deleteLap(RepNumber,LapNumber);
            isRunning=true;
            isRestRunning=false;
            if(modeMilliseconds)
                rest.setText(getString(R.string.milliseconds));
            else
                rest.setText(getString(R.string.centiseconds));
        }
        else if (isRunning&&LapNumber!=-1){
            deleteLap(RepNumber,LapNumber);
        }
        else if (isRunning&&RepNumber==0){
            clockReset(RepNumber);
            resetAndEditTimer.setVisibility(View.GONE);
        }
        else if (isRunning){
            deleteRep(RepNumber);
        }
        else{

        }
    }

    public void clockReset(int repToReset){
        currentTime = SystemClock.uptimeMillis();
        times.get(repToReset).set(START_TIME, currentTime);
        isRunning=false;
        isRestRunning=false;
        if(modeMilliseconds){
            rest.setText(getString(R.string.milliseconds));
            lap.setText(getString(R.string.milliseconds));
            timer.setText(getString(R.string.milliseconds));
        }
        else{
            rest.setText(getString(R.string.centiseconds));
            lap.setText(getString(R.string.centiseconds));
            timer.setText(getString(R.string.centiseconds));
        }
    }

    public void plusOne(View view){
        changeTime(1000);
    }

    public void minusOne(View view){
        currentTime = SystemClock.uptimeMillis();
        long temp1 = currentTime-times.get(RepNumber).get(START_TIME);
        if(temp1 < 1000)
            Snackbar.make(stopwatchActivity,"The timer does not have enough time on it to subtract a full second.",MESSAGE_DISPLAY_LENGTH).show();
        else if(RepNumber>0){
            long temp2 = times.get(RepNumber-1).get(END_OF_REST);
            if(temp2 < 1000)
                Snackbar.make(stopwatchActivity,"The rest on the previous rep was too short to subtract a full second.",MESSAGE_DISPLAY_LENGTH).show();
        }
        else
            changeTime(-1000);
    }

    public void changeTime(int ms){
        vibrate(100);
        selectRep(RepNumber);
        times.get(RepNumber).set(START_TIME, times.get(RepNumber).get(START_TIME)-ms);
        /*for(int i = 0; i <= LapNumber; i++){
            laps.get(RepNumber).set(i, laps.get(RepNumber).get(i) - ms);
        }*/
        if(RepNumber>0){
            times.get(RepNumber-1).set(END_OF_REST, times.get(RepNumber).get(END_OF_REST) - ms);
        }
        updateDisplay();
    }
    public void updateDisplay(){
        if(LapNumber>-1)
            lapItemAdapters.get(RepSelected).notifyDataSetChanged();
        if(!isRunning&&isRestRunning){
            timer.setText(getTimeString(times.get(RepNumber).get(STOP_TIME) - times.get(RepNumber).get(START_TIME)));
            lap.setText(getTimeString(times.get(RepNumber).get(STOP_TIME)-laps.get(RepNumber).get(LapNumber)));
        }
    }

    public void clickEdit(View view){
    }


    public Runnable runnable = new Runnable() {
        public void run() {
            currentTime = SystemClock.uptimeMillis();
            if(isRunning){
                timer.setText(getTimeString(currentTime - times.get(RepNumber).get(START_TIME)));
                if(LapNumber!=-1)
                    lap.setText(getTimeString(currentTime - laps.get(RepNumber).get(LapNumber)));
                else
                    lap.setText(timer.getText().toString());
            }
            else if(isRestRunning)
                rest.setText(getTimeString(currentTime - times.get(RepNumber).get(1)));


            handler.postDelayed(this, 0);
        }
    };

    public String getTimeString(long time){
        int seconds = (int) (time / 1000);
        int minutes = seconds / 60;
        seconds = seconds % 60;
        int milliseconds = (int) (time % 1000);

        String string = String.format(Locale.US,"%02d", minutes) + ":" + String.format(Locale.US,"%02d", seconds) + ".";
        if(modeMilliseconds)
            string += String.format(Locale.US,"%03d", milliseconds);
        else
            string += String.format(Locale.US,"%02d", milliseconds /10);

        return string;

    }

    public void vibrate(int ms){
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            v.vibrate(VibrationEffect.createOneShot(ms,VibrationEffect.DEFAULT_AMPLITUDE));
        else
            v.vibrate(ms);

    }
/*
    class ClockStartTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if(RepSelected != RepNumber)
                    selectRep(RepNumber);
                currentTime = SystemClock.uptimeMillis();
                if(isRestRunning) {
                    times.get(RepNumber).set(END_OF_REST, currentTime);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rest.setText(getTimeString(0L));
                            addRep();
                        }
                    });
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        times.get(RepNumber).set(START_TIME, currentTime);
                        handler.postDelayed(runnable, 0);
                        isRunning = true;
                        isRestRunning = false;
                    }
                });
            }
            catch (final Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(stopwatchActivity, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", MESSAGE_DISPLAY_LENGTH).show();
                        System.out.println(e.toString());
                    }
                });
                this.cancel(true);
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void param){

        }
    }

    class ClockStopTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                new ClockLapTask().execute();
                if(RepSelected!=RepNumber)
                    selectRep(RepNumber);
                currentTime = SystemClock.uptimeMillis();
                times.get(RepNumber).set(STOP_TIME, currentTime);
                isRunning = false;
                isRestRunning = true;
            }
            catch (final Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(stopwatchActivity, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", MESSAGE_DISPLAY_LENGTH).show();
                        System.out.println(e.toString());
                    }
                });
                this.cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param){

        }
    }
    class ClockLapTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if(isRunning){
                    if(RepSelected!=RepNumber)
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                selectRep(RepNumber);
                                System.out.println(1);
                            }
                        });

                    currentTime = SystemClock.uptimeMillis();

                    if(LapNumber!=-1)
                        lapItemArrayLists.get(RepSelected).add(new LapItem(timer.getText().toString(), getDiffString(laps.get(RepNumber).get(LapNumber), currentTime)));
                    else
                        lapItemArrayLists.get(RepSelected).add(new LapItem(timer.getText().toString(),getString(R.string.nulldiff)));

                    laps.get(RepNumber).add(currentTime);
                    LapNumber++;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lapItemAdapters.get(RepSelected).notifyDataSetChanged();
                            lapLists.get(RepSelected).smoothScrollToPosition(lapLists.get(RepSelected).getBottom());
                        }
                    });
                }
            }
            catch (final Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(stopwatchActivity, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", MESSAGE_DISPLAY_LENGTH).show();
                        System.out.println(e.toString());
                    }
                });
                this.cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param){
        }
    }*/
}