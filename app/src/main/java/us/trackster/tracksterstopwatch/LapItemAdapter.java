package us.trackster.tracksterstopwatch;


import android.content.Context;
import android.support.annotation.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LapItemAdapter extends ArrayAdapter<LapItem> {
    private Context lapItemContext;
    private List<LapItem> lapItemList = new ArrayList<>();
    public LapItemAdapter(@NonNull Context context, ArrayList<LapItem> list) {
        super(context, 0 , list);
        lapItemContext = context;
        lapItemList = list;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(lapItemContext).inflate(R.layout.lap_list,parent,false);
        LapItem currentLapItem = lapItemList.get(position);
        TextView lap = (TextView) listItem.findViewById(R.id.textLap);
        lap.setText(currentLapItem.getLapTime());
        TextView diff = (TextView) listItem.findViewById(R.id.textDiff);
        diff.setText(currentLapItem.getDiff());
        //TextView diff = (TextView) listItem.findViewById();
        //diff.setText(currentLapItem.getDiff());
        return listItem;
    }
}